import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Calculation {

    public BlockingQueue<Double> queue = new ArrayBlockingQueue<Double>(10);
    public CompletableFuture<Double> result = new CompletableFuture<>();

    Producer producer = new Producer( queue, 1000L);
    Consumer consumer = new Consumer(queue, result);

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Calculation calculation = new Calculation();
        calculation.calculate();
    }

    public void calculate() throws InterruptedException, ExecutionException {
        producer.start();
        consumer.start();
        producer.join();
        consumer.join();
        System.out.println(result.get() * 4);
    }
}


