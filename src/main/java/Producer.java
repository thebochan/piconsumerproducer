import java.util.concurrent.BlockingQueue;

public class Producer extends Thread {
    private Calculation worker;
    private BlockingQueue<Double> queue;

    Producer(BlockingQueue<Double> queue, Long n) {
        this.n = n;
        this.queue = queue;
    }

    private int i = 0;
    private long n;

    private boolean cancelled = false;

    private void cancel() {
        cancelled = true;
    }

    public void run() {
        try {
            if (!cancelled){
                produce();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void produce() throws InterruptedException {
        while (i <= n) {
            double formulaElement = Math.pow(-1, i) / (2 * i + 1);
            queue.put(formulaElement);
            i++;
        }
        cancel();
    }
}