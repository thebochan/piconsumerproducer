import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class Consumer extends Thread {

    private Calculation worker;
    private CompletableFuture<Double> result;
    private Double resultTemp;
    private BlockingQueue<Double> queue;
    private boolean cancelled = false;

    public Consumer(BlockingQueue<Double> queue, CompletableFuture<Double> result){
        this.queue = queue;
        this.result = result;
    }

    public void run() {
        resultTemp = 0d;
        try {
            consume();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void consume() throws InterruptedException {
        while (true){
            Double element = queue.poll(1L, TimeUnit.SECONDS);
            if (element == null){
                System.out.println("ended");
                break;
            } else {
                System.out.println("Another element = " + element);
                resultTemp += element;
            }
        }
        result.complete(resultTemp);
    }
}
